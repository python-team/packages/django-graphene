django-graphene (3.2.2-2) unstable; urgency=medium

  * Team upload.
  * Remove extraneous dependency on old python3-singledispatch backport.

 -- Alexandre Detiste <tchet@debian.org>  Sun, 15 Sep 2024 15:18:33 +0200

django-graphene (3.2.2-1) unstable; urgency=medium

  * [72cdaa5] New upstream version 3.2.2
  * [4cc95ac] d/control: Update Standards-Version to 4.7.0

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sat, 15 Jun 2024 08:16:44 +0200

django-graphene (3.2.1-1) unstable; urgency=medium

  * [7439a41] New upstream version 3.2.1

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sat, 20 Apr 2024 09:52:00 +0200

django-graphene (3.2.0-1) unstable; urgency=medium

  [ Alexandre Detiste ]
  * [94b7367] remove extraneous build-dep python3-mock

  [ Carsten Schoenert ]
  * [d885ffa] d/watch: Add compression type gz
  * [a2e3e5e] New upstream version 3.2.0
  * [260a2ae] d/copyright: Update year data

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sat, 20 Jan 2024 08:07:15 +0100

django-graphene (3.1.5-1) unstable; urgency=medium

  * [e5010d7] New upstream version 3.1.5

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sat, 12 Aug 2023 07:02:41 +0200

django-graphene (3.1.3-1) unstable; urgency=medium

  * [26b4423] New upstream version 3.1.3

 -- Carsten Schoenert <c.schoenert@t-online.de>  Fri, 21 Jul 2023 18:19:15 +0200

django-graphene (3.1.2-1) unstable; urgency=medium

  * [b9aa42d] New upstream version 3.1.2
  * [c9bf9d5] Rebuild patch queue from patch-queue branch
    Updated patch:
    Sideloading-Don-t-link-to-external-resources.patch
  * [0711107] debian/*: Update file data about shipped JS files
  * [320110d] d/copyright: Update data on CSS files
  * [ddc1ad0] d/README.source: Update information about included CSS

 -- Carsten Schoenert <c.schoenert@t-online.de>  Wed, 21 Jun 2023 19:52:29 +0200

django-graphene (3.1.1-1) experimental; urgency=medium

  * [8462a31] New upstream version 3.1.1
  * [50ce3f1] Rebuild patch queue from patch-queue branch
    Updated patch:
    Sideloading-Don-t-link-to-external-resources.patch
    Removed patch (fixed upstream):
    docs-settings.rst-Correct-reST-syntax.patch
  * [b5e315a] debian/*: Update file data about shipped JS files
  * [7a63818] d/README.source: Adjust information about *.js files

 -- Carsten Schoenert <c.schoenert@t-online.de>  Mon, 29 May 2023 14:04:46 +0200

django-graphene (3.0.2-1) experimental; urgency=medium

  * [c4d8bf1] New upstream version 3.0.2

 -- Carsten Schoenert <c.schoenert@t-online.de>  Thu, 04 May 2023 17:28:17 +0200

django-graphene (3.0.1-1) experimental; urgency=medium

  * [801b377] New upstream version 3.0.1
  * [893dec8] Rebuild patch queue from patch-queue branch
    Updated patch:
    Sideloading-Don-t-link-to-external-resources.patch
  * [e5e33c0] debian/*: Update file data about shipped JS files
    Upstream dropped the usage of 'client.js' and uses #graphql-ws.min.js'
    now. Updating the files in debian/{missing-sources,static}.
  * [20fdb16] d/python3-django-graphene: Drop install of client.js
  * [7d29514] d/copyright: Update data about files
  * [e471c61] d/README.source: Adjust information about *.js files
  * [b151fa2] Lintian: Adjust lintian overriding rules

 -- Carsten Schoenert <c.schoenert@t-online.de>  Wed, 03 May 2023 19:21:53 +0200

django-graphene (3.0.0-1) experimental; urgency=medium

  * [b5949dc] New upstream version 3.0.0
  * [d24cc5a] Rebuild patch queue from patch-queue branch
    Modified/renamed patch:
    settings.rst-Correct-reST-syntax.patch
    -> docs-settings.rst-Correct-reST-syntax.patch
    Modified/renamed patch:
    Don-t-use-intersphinx-while-build-of-documentation.patch
    -> docs-Use-local-inventory-for-intersphinx.patch
  * [9b257db] d/control: Adjust B-D entries, add BuildProfileSpecs
  * [9fed188] Lintian: Adjust syntax for source package processing
  * [a9d14d9] Lintian: Adjust/update *-doc package entries
  * [df0c1e6] Lintian: Adjust/update python3-* package entries
  * [bc57a09] d/rules: Create a reproducible doc package
  * [db840c7] d/rules: Drop --with option in default target
  * [5d7eb3b] d/control: Update Standards-Version to 4.6.2
    No further changes needed.
  * [8484a8e] d/copyright: Update year data
  * [d86ea3f] d/README.source: Update, add info about pygments-graphql

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sat, 25 Feb 2023 11:28:34 +0100

django-graphene (2.15.0-2) unstable; urgency=medium

  * [51d2c05] d/control: Correct Homepage URL data
  * [ba6b1fa] Rebuild patch queue from patch-queue branch
    Added patch:
    Utils-Switch-import-depend-on-Django-version.patch

 -- Carsten Schoenert <c.schoenert@t-online.de>  Wed, 13 Jul 2022 21:33:53 +0200

django-graphene (2.15.0-1) unstable; urgency=medium

  * Upload to unstable
  * [2240bec] d/control: Remove obsolete field Testsuite
  * [ab70a1f] d/control: Update Standards-Version to 4.6.0
    No further changes needed.
  * [5dec035] d/rules: Remove unneeded lines
  * [e9ab76d] d/README.source: Add hint about req. local installed package
    In case you build this package by git-pbuilder ypu will need to have
    the package python3-pytest-runner installed locally.
  * [318a8a1] d/control: Mark the -doc package Multi-Arch: foreign
  * [dccfb96] d/s/lintian-overrides: Correct and adjust the entries
  * [c997101] d/gbp.conf: Adjust to branch debian/master

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sun, 09 Jan 2022 18:34:54 +0100

django-graphene (2.15.0-1~exp1) experimental; urgency=medium

  * [000b43f] New upstream version 2.15.0
  * [43b756c] First and basic Debianization
    (Closes: #991812)

 -- Carsten Schoenert <c.schoenert@t-online.de>  Wed, 18 Aug 2021 18:01:27 +0200
