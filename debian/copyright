Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: graphene-django
Upstream-Contact: Syrus Akbary <me@syrusakbary.com>
Source: https://github.com/graphql-python/graphene-django
Comment: Note that some additional CSS and JS files are shipped within the
 debian/ folder to prevent later side loading of the required files in the
 Django application.
 More information on this can be found in debian/README.source.

Files: *
Copyright: Syrus Akbary <me@syrusakbary.com>
License: MIT

Files: debian/*
Copyright: 2021-2024, Carsten Schoenert <c.schoenert@t-online.de>
License: MIT

Files: debian/missing-sources/graphql-ws.js
 debian/static/graphql-ws.min.js
Copyright: 2020-2021 Denis Badurina
License: MIT
Comment: According to https://github.com/enisdenjo/graphql-ws/blob/master/LICENSE.md

Files: debian/static/graphiql.min.css
 debian/static/style.css
 debian/static/graphiql.min.js
 debian/static/graphiql-plugin-explorer.umd.js
 debian/missing-sources/graphiql.css
 debian/missing-sources/style.ccs
 debian/missing-sources/graphiql.js
 debian/missing-sources/graphiql-plugin-explorer.es.js
Copyright: Rikki Schulte <rikki.schulte@gmail.com>
 Samuel <samuelimolo4real@gmail.com>
 Yoshi Sukeda <yoshisukeda@yahoo.com>
License: MIT
Comment: According to https://github.com/graphql/graphiql/blob/main/LICENSE

Files: debian/static/fetch.umd.js
Copyright: Various contributors
License: MIT
Comment: According to https://github.com/github/fetch/blob/master/LICENSE

Files: debian/static/react.production.min.js
 debian/static/react-dom.production.min.js
 debian/missing-sources/react.development.js
 debian/missing-sources/react-dom.development.js
Copyright: Facebook
 Various contributors
License: MIT
Comment: According to https://github.com/facebook/react/blob/main/AUTHORS
 and
 https://github.com/facebook/react/blob/main/LICENSE

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
